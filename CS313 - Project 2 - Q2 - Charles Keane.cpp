// CS313Proj2 - Q2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>

using namespace std;

int EvaluatePrefixExpression(vector<char> exp);
vector<char> ConvertExpressionToPrefix(string exp, int length);

int main()
{
    string expression = "2+4="; // + 2 3
    int length = 0;
    int i = 0;
    char c = expression[i];

    while (c != '=') { //Calculating the length of the expression, was going to use this for some kind of tracking
        length++;
        i++;
        c = expression[i];
    }

    vector<char> convertedString = ConvertExpressionToPrefix(expression, length); //This method is to convert the expression from infix to prefix format
    cout << EvaluatePrefixExpression(convertedString);
}

vector<char> ConvertExpressionToPrefix(string exp, int length) {
    vector<char> v1; //A vector of chars to build our prefix expression on the stack
    int i = 0;
    
    int* listArray = new int[length]; //This array was going to keep track of which elements in the expression have already been accounted for to prevent adding something more than once, but ended up not using it. The array was also created this way to use a variable instead of a constant as its length
    int listCount = 0;

    i = 0;
    char c = exp[i];
    while (c != '=') { //The equal sign is where we end
        if (c == '*' || c == '/' || c == '+' || c == '-') { //When we find an operator, we push the operator first since this is prefix format. If it were postfix, we'd push operands one and two first then the sign
            v1.push_back(exp[i]);
            listArray[listCount] = i; //As mentioned above this was going to keep track of which elements were accounted for already
            listCount++;
            v1.push_back(exp[i - 1]);
            listArray[listCount] = i-1;
            listCount++;
            v1.push_back(exp[i + 1]);
            listArray[listCount] = i+1;
            listCount++;
        }
        i++;
        c = exp[i];
    }

    delete[] listArray; //Deleting the array created above to avoid a memory leak.
    return v1;
}

int EvaluatePrefixExpression(vector<char> exp) {
    int result = 0;
    int op1 = 0;
    int op2 = 0;
    char op;

    while (exp.size() > 1) {
        if (exp.front() == '+' || exp.front() == '-' || exp.front() == '*' || exp.front() == '/') {
            op2 = exp.back() - '0';
            exp.pop_back();
            op1 = exp.back() - '0';
            exp.pop_back();
            op = exp.back();
            exp.pop_back();
        }

        if (op == '+') { //Different cases to provide different operations
            result = op1 + op2;
        }
        else if (op == '-') {
            result = op1 - op2;
        }
        else if (op == '*') {
            result = op1 * op2;
        }
        else {
            if (op2 != 0) {
                result = op1 / op2;
            }
            else {
                cout << "Cannot divide by 0!";
            }
        }
    }

    return result;
}