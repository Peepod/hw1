#include <iostream>
#include <chrono>
#include <vector>

using namespace std;
using std::chrono::steady_clock;
using std::chrono::duration_cast;
using std::chrono::seconds;
using std::chrono::milliseconds;

template <typename Func>
long long TimeFunc(Func f)
{
    auto begin = steady_clock::now();
    f();
    auto end = steady_clock::now();

    return duration_cast<milliseconds>(end - begin).count();
}

struct Node { //Lines 20 to 37 were taken from https://www.tutorialspoint.com/cplusplus-program-to-implement-stack-using-linked-list with comments added by me
    int data; //Stores the data of the node
    struct Node* next; //A pointer that points to the next node in the linked stack
};
struct Node* top = NULL;
void push(int val) {
    struct Node* newnode = (struct Node*)malloc(sizeof(struct Node));
    newnode->data = val; //Creates a new node, has one parameter that takes in an integer and stores the value in the Node's data variable.
    newnode->next = top; //Setting this new node to be the top of the stack
    top = newnode;
}
void pop() {
    if (top == NULL)
        cout << "Stack Underflow" << endl; //Cannot pop nothing
    else {
        top = top->next; //Sets the 2nd highest node to be the top since the top is being popped off
    }
}

void FillLinkedStack();
void FillArrayStack();

int main() {
    int ch, val;

    auto linkedListStackTime = TimeFunc([&]() {FillLinkedStack(); }); //Running each function and storing the times in their respective variables.
    auto arrayStackTime = TimeFunc([&]() {FillArrayStack(); });

    cout << "Linked List Stack Time: " << linkedListStackTime << "ms" << endl;

    cout << "Array Stack Time: " << arrayStackTime << "ms" << endl;
}

//The vector takes about 2.2x longer compared to a linked list stack

void FillLinkedStack() {
    for (int i = 0; i < 10000000;i++) {
        push(1000); //Pushing the number 1000 ten million times in a linked list stack
    }
}

void FillArrayStack() {
    vector<int> v1;
    for (int i = 0; i < 10000000;i++) {
        v1.push_back(1000); //Pushing the number 1000 ten million times in a vector
    }
}