// CS313 Project1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#include <list>
#include <chrono>
#include <utility>
using namespace std;
using std::chrono::steady_clock;
using std::chrono::duration_cast;
using std::chrono::seconds;
using std::chrono::milliseconds;

void FillVectorNumbers(vector<int>& x);
void FillListNumbers(list<int>& x);
void FillVectorAscii(vector<string>& x);
void FillListAscii(list<string>& x);
void MoveVectorAscii(vector<string>&& x);
void MoveListAscii(list<string>&& x);
void GenerateArray(int x[], int size);
int RecursiveBinary(int x[], int start, int end, int sought);
int binarySearch(int arr[], int l, int r, int x);
int IterativeBinary(int x[], int start, int end, int sought);
int IterativeBinarySearch2(int arr[], int l, int r, int x);
void BubbleSort(int x[][10]);
void print(int print[][10]);
void FillTwoDimArray(int x[][10]);
void InsertionSort(int x[][10]);
void BubbleSort2(int x[][100]);
void FillTwoDimArrayQ4(int x[][100]);
void FillListNumbersQ4(list<int>& x);
void BubbleSortList(list<int> x);

template <typename Func>
long long TimeFunc(Func f)
{
    auto begin = steady_clock::now();
    f();
    auto end = steady_clock::now();

    return duration_cast<milliseconds>(end - begin).count();
}


int main()
{
    srand(time(NULL));

    //QUESTION 1
    
    vector<int> vecNumbers; //Create an int vector with nothing in it
    list<int> listNumbers; //Create an int list with nothing in it

    vector<string> vecString; //Create a string vector with nothing in it
    list<string> listString; //Create a string list with nothing in it

    auto vecNumbersMilliSeconds = TimeFunc([&]() {FillVectorNumbers(vecNumbers); });
    auto listNumbersMilliSeconds = TimeFunc([&]() {FillListNumbers(listNumbers); });

    auto vecStringsMilliSeconds = TimeFunc([&]() {FillVectorAscii(vecString); });
    auto listStringsMilliSeconds = TimeFunc([&]() {FillListAscii(listString); });

    auto test = TimeFunc([&]() {FillVectorAscii(vecString); });
    auto test2 = TimeFunc([&]() {FillListAscii(listString); });
    auto vecStringsMovedMilliSeconds = TimeFunc([&]() {MoveVectorAscii(move(vecString)); });
    auto listStringsMovedMilliSeconds = TimeFunc([&]() {MoveListAscii(move(listString)); });

    //QUESTION 1A
    cout << "Time to fill randomly generated numbers in a Vector: " << vecNumbersMilliSeconds << "ms" << endl;

    cout << "Time to fill randomly generated numbers in a List: " << listNumbersMilliSeconds << "ms" << endl;

    //QUESTION 1B
    cout << "Time to fill randomly generated strings in a Vector: " << vecStringsMilliSeconds << "ms" << endl;

    cout << "Time to fill randomly generated strings in a List: " << listStringsMilliSeconds << "ms" << endl;

    //QUESTION 1C

    cout << "Time to pass in the previously generated vector of strings into the function using copy: " << test << "ms" << endl;
    cout << "Time to pass in the previously generated vector of strings into the function using move: " << vecStringsMovedMilliSeconds << "ms" << endl;

    cout << "Time to pass in the previously generated list of strings into the function using copy: " << test2 << "ms" << endl;
    cout << "Time to pass in the previously generated list of strings into the function using move: " << listStringsMovedMilliSeconds << "ms" << endl;
    

    
    //QUESTION 2
    int* array1 = new int[1000000];
    int* array2 = new int[10000000];
    int* array3 = new int[100000000];

    GenerateArray(array1, 1000000);
    GenerateArray(array2, 10000000);
    GenerateArray(array3, 100000000);

    //auto Recursive1Mil = TimeFunc([&]() {RecursiveBinary(array1,0,x-1,2000); });
    //auto Recursive10Mil = TimeFunc([&]() {RecursiveBinary(array2, 0, y-1, 2000); });
    //auto Recursive100Mil = TimeFunc([&]() {RecursiveBinary(array3, 0, z-1, 2000); });

    auto Recursive1Mil = TimeFunc([&]() {binarySearch(array1, 0, 1000000 - 1, 765700); });
    auto Recursive10Mil = TimeFunc([&]() {binarySearch(array2, 0, 10000000 - 1, 765700); });
    auto Recursive100Mil = TimeFunc([&]() {binarySearch(array3, 0, 100000000-1, 765700); });

    cout << "Time to search Array of size 1,000,000 via Recursive Binary Search:" << Recursive1Mil << "ms" << endl;
    cout << "Time to search Array of size 10,000,000 via Recursive Binary Search:" << Recursive10Mil << "ms" << endl;
    cout << "Time to search Array of size 100,000,000 via Recursive Binary Search:" << Recursive100Mil << "ms" << endl;

    auto Iterative1Mil = TimeFunc([&]() {IterativeBinarySearch2(array1, 0, 1000000 - 1, 765700); });
    auto Iterative10Mil = TimeFunc([&]() {IterativeBinarySearch2(array2, 0, 10000000 - 1, 765700); });
    auto Iterative100Mil = TimeFunc([&]() {IterativeBinarySearch2(array3, 0, 100000000 - 1, 765700); });

    cout << "Time to search Array of size 1,000,000 via Iterative Binary Search:" << Iterative1Mil << "ms" << endl;
    cout << "Time to search Array of size 10,000,000 via Iterative Binary Search:" << Iterative10Mil << "ms" << endl;
    cout << "Time to search Array of size 100,000,000 via Iterative Binary Search:" << Iterative100Mil << "ms" << endl;
    

    //QUESTION 3
    /* //This was for testing
    int Array2D[3][3];
    Array2D[0][0] = 99;
    Array2D[0][1] = 1;
    Array2D[0][2] = 5;
    Array2D[1][0] = 24;
    Array2D[1][1] = 2;
    Array2D[1][2] = 7;
    Array2D[2][0] = 2;
    Array2D[2][1] = 8;
    Array2D[2][2] = 17;
    */

    int Array2D[10][10];
    FillTwoDimArray(Array2D);

    BubbleSort(Array2D);

    //print(Array2D); //For testing

    //QUESTION 4

    int ArrayQ4[100][100];
    FillTwoDimArrayQ4(ArrayQ4);

    list<int> listNumbersQ4;
    FillListNumbersQ4(listNumbersQ4);

    auto RandomArrayTimeQ4 = TimeFunc([&]() {BubbleSort2(ArrayQ4); });
    auto RandomListTimeQ4 = TimeFunc([&]() {BubbleSortList(listNumbersQ4); });

    cout << "Time to sort a random array via Bubble Sort:" << RandomArrayTimeQ4 << "ms" << endl;
    cout << "Time to sort a random list via Bubble Sort:" << RandomListTimeQ4 << "ms" << endl;
}

/*
class node {
public:
    int info;
    node* link;
}*/

#pragma region Question1Functions
void FillVectorNumbers(vector<int>& x) {
    for (int i = 0; i < 1000000;i++) { //In this for loop we add one million randomly generated numbers from 1 to 1000 to a vector.
        x.push_back(rand() % 1000 + 1);
    }

    //cout << x.at(4); For testing
}

void FillListNumbers(list<int>& x) {
    for (int i = 0; i < 1000000;i++) { //In this for loop we add one million randomly generated numbers from 1 to 1000 to a list.
        x.push_back(rand() % 1000 + 1);
    }
}

void FillVectorAscii(vector<string>& x) {
    char c;
    string s;
    for (int i = 0; i < 1000000; i++)
    {
        s.clear(); //This makes sure the string doesn't contain any characters before populating it with randomly generated characters
        for (int j = 0; j < 10; j++) {
            c = rand() % 128 + 'a';
            s += c;
        }
        x.push_back(s); //Pushes the generated string onto the string vector
    }

    /* //For testing
    for (int i = 0; i < x.size();i++) {
        cout << x.at(i) << endl;
    }
    */
}

void FillListAscii(list<string>& x) {
    char c;
    string s;
    for (int i = 0; i < 1000000; i++)
    {
        s.clear(); //This makes sure the string doesn't contain any characters before populating it with randomly generated characters
        for (int j = 0; j < 10; j++) {
            c = rand() % 128 + 'a';
            s += c;
        }
        x.push_back(s); //Pushes the generated string onto the string list
    }
}

void MoveVectorAscii(vector<string>&& x) {
    char c;
    string s;
    for (int i = 0; i < 1000000; i++)
    {
        s.clear(); //This makes sure the string doesn't contain any characters before populating it with randomly generated characters
        for (int j = 0; j < 10; j++) {
            c = rand() % 128 + 'a';
            s += c;
        }
        x.push_back(s); //Pushes the generated string onto the string vector
    }
}

void MoveListAscii(list<string>&& x) {
    char c;
    string s;
    for (int i = 0; i < 1000000; i++)
    {
        s.clear(); //This makes sure the string doesn't contain any characters before populating it with randomly generated characters
        for (int j = 0; j < 10; j++) {
            c = rand() % 128 + 'a';
            s += c;
        }
        x.push_back(s); //Pushes the generated string onto the string vector
    }
}
#pragma endregion

#pragma region Question2Functions

void GenerateArray(int x[], int size) {
    for (int i = 0; i < size; i++)
    {
        x[i] = rand() % 2 + (i * 2);
    }
}

//This was a recursive binary search I attempted to write on my own. It was not giving me any compiler errors but it was also not working, so after failing for a good 30 minutes I am using one from GeeksForGeeks below, referenced here: https://www.geeksforgeeks.org/binary-search/
int RecursiveBinary(int x[], int start, int end, int sought) {
    int midNum = (start + (end - start)) / 2;
    if (x[midNum] == sought) {
        return midNum;
    }

    if (x[midNum] < sought) {
        RecursiveBinary(x, midNum + 1, end, sought);
    }
    else {
        RecursiveBinary(x, 1, midNum - 1, sought);
    }

    cout << "Number is not in the array!" << endl;
    return -1;
}

//As mentioned above, this function is taken from GeeksForGeeks after my own attempt to write the function would not work
int binarySearch(int arr[], int l, int r, int x)
{
    if (r >= l) {
        int mid = l + (r - l) / 2;

        if (arr[mid] == x)
            return mid;

        if (arr[mid] > x)
            return binarySearch(arr, l, mid - 1, x);

        return binarySearch(arr, mid + 1, r, x);
    }

    return -1;
}

int IterativeBinary(int x[], int start, int end, int sought) {

    while (start <= end) {
        int midNum = (start + (end - start)) / 2;
        
        if (x[midNum] == sought) {
            return midNum;
        }

        if (x[midNum] < sought) { //This means the number is to the right of the mid point, also the midpoint itself is not the number, so we set the starting point equal to the midpoint plus 1
            start = midNum + 1;
        
        }
        else { //Same logic as above but the number is to the left, so we set the end point equal to midpoint minus one.
            end = midNum - 1;
        }
    }

    return -1;
}

//I had issues getting my own attempt at writing an iterative binary search function (Shown above this function) to work. This is taken from GeeksForGeeks.
int IterativeBinarySearch2(int arr[], int l, int r, int x)
{
    while (l <= r) {
        int m = l + (r - l) / 2;
        if (arr[m] == x)
            return m;
        if (arr[m] < x)
            l = m + 1;
        else
            r = m - 1;
    }

    return -1;
}

#pragma endregion

#pragma region Question3Functions
void FillTwoDimArray(int x[][10]) {
    for (int i = 0;i < 10;i++) {
        for (int j = 0;j < 10;j++) {
            x[i][j] = rand() % 1000;
        }
    }
}



void BubbleSort(int x[][10]) {
    int changeCount = 1;

    while (changeCount > 0) { //Letting the while loop only continue if a change/swap has been made. If it gets to the end and no swaps have been made the array is sorted and we don't need to go through it again
        changeCount = 0;
        for (int i = 0;i < 10;i++) {
            for (int j = 0;j < 10;j++) {
                if (j < 10 - 1) { //If we are in the last column then we need to compare to the next row and first column, this checks for that
                    if (x[i][j] > x[i][j + 1]) {
                        swap(x[i][j], x[i][j + 1]);
                        changeCount++;
                    }
                }
                if (j == 10-1 && i != 10 - 1) { //This checks if we are in the last column, but also checks we aren't in the last row. Otherwise we would go out of bounds
                    if (x[i][j] > x[i + 1][0]) {
                        swap(x[i][j], x[i + 1][0]);
                        changeCount++;
                    }
                }
            }
        }
    }
}



//Struggling to figure this out, will try to reevaluate later
void InsertionSort(int x[][10]) {
    
    int compare,k;
    int y[10][10];
    y[0][0] = x[0][0];
    compare = y[0][0];
    for (int i = 0; i < 10; i++) {

        for (int j = 0; j < 10;j++) {
            if (x[i][j] < compare) {
                
            }
        }
    }
}

void swap(int& x, int& y) {
    int temp = x;
    x = y;
    y = temp;
}

void print(int print[][10])
{
    for (int i = 0; i < 10; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            cout << print[i][j] << endl;
        }
    }
}
#pragma endregion

#pragma region Question4Functions
/*
void CreateListQ4(list<int>& x) {
    for (int i = 0; i < 10000;i++) {
        node* first = new node;

    }
}*/

void FillListNumbersQ4(list<int>& x) {
    for (int i = 0; i < 10000;i++) { 
        x.push_back(rand() % 1000 + 1);
    }
}

void FillTwoDimArrayQ4(int x[][100]) {
    for (int i = 0;i < 100;i++) {
        for (int j = 0;j < 100;j++) {
            x[i][j] = rand() % 1000;
        }
    }
}

void BubbleSort2(int x[][100]) {
    int changeCount = 1;

    while (changeCount > 0) { //Letting the while loop only continue if a change/swap has been made. If it gets to the end and no swaps have been made the array is sorted and we don't need to go through it again
        changeCount = 0;
        for (int i = 0;i < 100;i++) {
            for (int j = 0;j < 100;j++) {
                if (j < 100 - 1) { //If we are in the last column then we need to compare to the next row and first column, this checks for that
                    if (x[i][j] > x[i][j + 1]) {
                        swap(x[i][j], x[i][j + 1]);
                        changeCount++;
                    }
                }
                if (j == 100 - 1 && i != 100 - 1) { //This checks if we are in the last column, but also checks we aren't in the last row. Otherwise we would go out of bounds
                    if (x[i][j] > x[i + 1][0]) {
                        swap(x[i][j], x[i + 1][0]);
                        changeCount++;
                    }
                }
            }
        }
    }
}

//I'm not sure how to access the nodes in the std::list. I know that with linked lists we can't randomly access and need to iterate from the beginning.
void BubbleSortList(list<int> x) {
    int changeCount = 1;
    //list<int> iterator it = find(l.begin(), l.end(), 7);
    while (changeCount > 0) { //Letting the while loop only continue if a change/swap has been made. If it gets to the end and no swaps have been made the array is sorted and we don't need to go through it again
        changeCount = 0;
        for (int i = 0;i < 10000;i++) {

        }
    }
}
#pragma endregion




