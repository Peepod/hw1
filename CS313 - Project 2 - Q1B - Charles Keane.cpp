//Header File: linkedStack.h 

#ifndef H_StackType
#define H_StackType

#include <iostream> //Used for input/output streaming like cin/cout 
#include <cassert> //Let's us assert, which presumes something is true and if it isn't terminates the program

#include "stackADT.h"

using namespace std;

//Definition of the node 
template <class Type>
struct nodeType //Generic class called nodeType which includes info and a link to the next node in this linked list
{
    Type info;
    nodeType<Type>* link;
};

template <class Type>
class linkedStackType : public stackADT<Type>
{
public:
    const linkedStackType<Type>& operator=
        (const linkedStackType<Type>&);
    //Overload the assignment operator.

    bool isEmptyStack() const;
    //Function to determine whether the stack is empty.
    //Postcondition: Returns true if the stack is empty;
    //               otherwise returns false.

    bool isFullStack() const;
    //Function to determine whether the stack is full.
    //Postcondition: Returns false.

    void initializeStack();
    //Function to initialize the stack to an empty state. 
    //Postcondition: The stack elements are removed; 
    //               stackTop = nullptr;

    void push(const Type& newItem);
    //Function to add newItem to the stack.
    //Precondition: The stack exists and is not full.
    //Postcondition: The stack is changed and newItem 
    //               is added to the top of the stack.

    Type top() const;
    //Function to return the top element of the stack.
    //Precondition: The stack exists and is not empty.
    //Postcondition: If the stack is empty, the program 
    //               terminates; otherwise, the top 
    //               element of the stack is returned.

    void pop();
    //Function to remove the top element of the stack.
    //Precondition: The stack exists and is not empty.
    //Postcondition: The stack is changed and the top 
    //               element is removed from the stack.

    linkedStackType();
    //Default constructor
    //Postcondition: stackTop = nullptr;

    linkedStackType(const linkedStackType<Type>& otherStack);
    //Copy constructor

    ~linkedStackType();
    //Destructor
    //Postcondition: All the elements of the stack are 
    //               removed from the stack.

private:
    nodeType<Type>* stackTop; //pointer to the stack

    void copyStack(const linkedStackType<Type>& otherStack);
    //Function to make a copy of otherStack.
    //Postcondition: A copy of otherStack is created and
    //               assigned to this stack.
};


//Default constructor
template <class Type>
linkedStackType<Type>::linkedStackType()
{
    stackTop = nullptr; //By default when creating a new linked stack type the top node should be nullptr because we haven't created a node yet.
}

template <class Type>
bool linkedStackType<Type>::isEmptyStack() const
{
    return(stackTop == nullptr);
} //end isEmptyStack

template <class Type>
bool linkedStackType<Type>::isFullStack() const
{
    return false;
} //end isFullStack

template <class Type>
void linkedStackType<Type>::initializeStack()
{
    nodeType<Type>* temp; //pointer to delete the node

    while (stackTop != nullptr)  //while there are elements in 
                              //the stack
    {
        temp = stackTop;    //set temp to point to the 
                            //current node
        stackTop = stackTop->link;  //advance stackTop to the
                                    //next node
        delete temp;    //deallocate memory occupied by temp
    }
} //end initializeStack


template <class Type>
void linkedStackType<Type>::push(const Type& newElement)
{
    nodeType<Type>* newNode;  //pointer to create the new node

    newNode = new nodeType<Type>; //create the node

    newNode->info = newElement; //store newElement in the node
    newNode->link = stackTop; //insert newNode before stackTop
    stackTop = newNode;       //set stackTop to point to the 
                              //top node
} //end push


template <class Type>
Type linkedStackType<Type>::top() const
{
    assert(stackTop != nullptr); //if stack is empty,
                              //terminate the program
    return stackTop->info;    //return the top element 
}//end top

template <class Type>
void linkedStackType<Type>::pop()
{
    nodeType<Type>* temp;   //pointer to deallocate memory

    if (stackTop != nullptr)
    {
        temp = stackTop;  //set temp to point to the top node

        stackTop = stackTop->link;  //advance stackTop to the 
                                    //next node
        delete temp;    //delete the top node
    }
    else
        cout << "Cannot remove from an empty stack." << endl; //Output message to inform you can't remove something that isn't there
}//end pop

template <class Type>
void linkedStackType<Type>::copyStack //We are using the method found in the linkedStackType namespace, the method is copyStack
(const linkedStackType<Type>& otherStack) //Using const keywork to prevent the variable from changing
{
    nodeType<Type>* newNode, * current, * last;

    if (stackTop != nullptr) //if stack is nonempty, make it empty
        initializeStack();

    if (otherStack.stackTop == nullptr) //This conditional statement is for when the stack is empty, the stackTop is nullptr in ohterStack so we set stackTop to nullptr as well.
        stackTop = nullptr;
    else
    {
        current = otherStack.stackTop;  //set current to point
                                   //to the stack to be copied

            //copy the stackTop element of the stack 
        stackTop = new nodeType<Type>;  //create the node

        stackTop->info = current->info; //copy the info
        stackTop->link = nullptr;  //set the link field of the
                                //node to nullptr
        last = stackTop;        //set last to point to the node
        current = current->link;    //set current to point to
                                    //the next node

            //copy the remaining stack
        while (current != nullptr) //If current is not nullptr then there are more nodes to be copied.
        {
            newNode = new nodeType<Type>; //Create a new node

            newNode->info = current->info; //newNode copies the info from the current node
            newNode->link = nullptr; //This is being pushed onto the top of the stack, so it has nothing to point to at the moment and should be set to nullptr
            last->link = newNode; //last was previously the top of the stack, it is now beneath newNode which is on top and last needs to point to newNode
            last = newNode; //last represents the top fo the stack, that is currently newNode which was just pushed onto the stack
            current = current->link; //To continue cycling through the whole stack in the while loop, we go to the next one. If there is another node, we'll loop through again but if it's nullptr then we are done with teh while loop.
        }//end while
    }//end else
} //end copyStack

    //copy constructor
template <class Type>
linkedStackType<Type>::linkedStackType(
    const linkedStackType<Type>& otherStack)
{
    stackTop = nullptr; //This is creating a new stack and copying the one input here as a parameter otherStack.
    copyStack(otherStack);
}//end copy constructor

    //destructor
template <class Type>
linkedStackType<Type>::~linkedStackType()
{
    initializeStack();
}//end destructor

    //overloading the assignment operator
template <class Type>
const linkedStackType<Type>& linkedStackType<Type>::operator=
(const linkedStackType<Type>& otherStack)
{
    if (this != &otherStack) //avoid self-copy
        copyStack(otherStack);

    return *this;
}//end operator=

#endif