//***********************************************************
// Author: D.S. Malik
//
// Program: Postfix Calculator  
// This program evaluates postfix expressions.
//***********************************************************

#include <iostream> //Used for input/output streaming like cin/cout  
#include <iomanip> //Also used for input and output with manipulator functions
#include <fstream> //For opening/writing to files
#include "mystack.h" //A header which includes info about stackType

using namespace std;

//Method declarations
void evaluateExpression(ifstream& inpF, ofstream& outF,
    stackType<double>& stack,
    char& ch, bool& isExpOk);
void evaluateOpr(ofstream& out, stackType<double>& stack,
    char& ch, bool& isExpOk);
void discardExp(ifstream& in, ofstream& out, char& ch);
void printResult(ofstream& outF, stackType<double>& stack,
    bool isExpOk);

int main()
{
    bool expressionOk;
    char ch;
    stackType<double> stack(100);
    ifstream infile; //Data will be read into this variable
    ofstream outfile; //Data will be save to the file associated with this variable

    infile.open("RpnData.txt");

    if (!infile) //If it can't open the file, this conditional statement gives a message stating so and terminates the program
    {
        cout << "Cannot open the input file. "
            << "Program terminates!" << endl;
        return 1;
    }

    outfile.open("RpnOutput.txt");

    outfile << fixed << showpoint; //Showpoint shows decimal point even if the number is an integer, e.g. 40.0
    outfile << setprecision(2); //This is from iomanip and is for formating the decimal to two places

    infile >> ch; //Reads in a character to the char variable ch
    while (infile) //Keeps going through each character of the infile until non left
    {
        stack.initializeStack();
        expressionOk = true;
        outfile << ch;

        evaluateExpression(infile, outfile, stack, ch,
            expressionOk); //Uses the method below to evaluate the expression
        printResult(outfile, stack, expressionOk); //Prints the result of the above evulation
        infile >> ch; //begin processing the next expression
    } //end while 

    infile.close(); //Closes the filestream for infile and outfile
    outfile.close();

    return 0;

} //end main


void evaluateExpression(ifstream& inpF, ofstream& outF, stackType<double>& stack, char& ch, bool& isExpOk)
{
    double num;

    while (ch != '=') //We want to evaluate everything up until the equal sign, equal sign is like the period that we've reached the end of the expression
    {
        switch (ch)
        {
        case '#':
            inpF >> num;
            outF << num << " ";
            if (!stack.isFullStack()) //Above we declared our stack size to be 100, as long as we've not reached this limit we can push on to continue evulating the expression
                stack.push(num);
            else //We've exceeded the 100 stack size and output a message while also terminating the program
            {
                cout << "Stack overflow. "
                    << "Program terminates!" << endl;
                exit(0);  //terminate the program
            }

            break;
        default:
            evaluateOpr(outF, stack, ch, isExpOk);
        }//end switch

        if (isExpOk) //if no error
        {
            inpF >> ch;
            outF << ch;

            if (ch != '#')
                outF << " ";
        }
        else
            discardExp(inpF, outF, ch);
    } //end while (!= '=')
} //end evaluateExpression


void evaluateOpr(ofstream& out, stackType<double>& stack,
    char& ch, bool& isExpOk)
{
    double op1, op2;

    if (stack.isEmptyStack())
    {
        out << " (Not enough operands)"; //No operands were included so we can't evulate the expression. isExpOk is set to false as a result
        isExpOk = false;
    }
    else
    {
        op2 = stack.top();
        stack.pop();

        if (stack.isEmptyStack())
        {
            out << " (Not enough operands)"; //No operands were included so we can't evulate the expression. isExpOk is set to false as a result
            isExpOk = false;
        }
        else
        {
            op1 = stack.top(); //We have several switch cases here to perform the correct operation depending which operand is on the stack.
            stack.pop();

            switch (ch)
            {
            case '+':
                stack.push(op1 + op2);
                break;
            case '-':
                stack.push(op1 - op2);
                break;
            case '*':
                stack.push(op1 * op2);
                break;
            case '/':
                if (op2 != 0) //Here we make sure the denominator is not 0, since we cannot divide by 0
                    stack.push(op1 / op2);
                else
                {
                    out << " (Division by 0)"; //Outputs that you're trying to divide by 0 and sets the expression to not be okay
                    isExpOk = false;
                }
                break;
            default:
                out << " (Illegal operator)"; //If some operator other than +,-,*,/ is used by mistake we output that it's an illegal operator and sets isExpOk to false.
                isExpOk = false;
            }//end switch
        } //end else
    } //end else
} //end evaluateOpr


void discardExp(ifstream& in, ofstream& out, char& ch)
{
    while (ch != '=')
    {
        in.get(ch); //This is from istream and is getting characters from the stream, storing them in the istream variable.
        out << ch;
    }
} //end discardExp

void printResult(ofstream& outF, stackType<double>& stack,
    bool isExpOk)
{
    double result;

    if (isExpOk) //if no error, print the result
    {
        if (!stack.isEmptyStack())
        {
            result = stack.top();
            stack.pop();

            if (stack.isEmptyStack())
                outF << result << endl; //Outputs the result which is at the top of the stack to the output file. In the other conditional cases something has gone wrong and output errors instead.
            else
                outF << " (Error: Too many operands)" << endl;
        } //end if
        else
            outF << " (Error in the expression)" << endl;
    }
    else
        outF << " (Error in the expression)" << endl;

    outF << "_________________________________"
        << endl << endl;
} //end printResult
